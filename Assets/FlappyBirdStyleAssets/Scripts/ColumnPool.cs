﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColumnPool : MonoBehaviour
{
    public int columnpoolsize = 5;
    private GameObject[] columns;
    public GameObject columnprefab;
    private Vector2 objectPoolPosition = new Vector2(-15f, -25f);
    public float columnmin = -1f;
    public float columnmax = 3.5f;
    private float timeSinceLastSpawned;
    public float spawnrate = 4f;
    private float spawnXposition = 10f;
    private int currentColumn = 0;
    // Start is called before the first frame update
    void Start()
    {
        columns = new GameObject[columnpoolsize];
        for (int i = 0; i < columnpoolsize; i++)
        {
            columns[i] = (GameObject)Instantiate(columnprefab, objectPoolPosition, Quaternion.identity);
        }
    }

    // Update is called once per frame
    void Update()
    {
        timeSinceLastSpawned += Time.deltaTime;
        if(GameControl.instance.gameOver== false && timeSinceLastSpawned >= spawnrate)
        {
            timeSinceLastSpawned = 0;
            float spawnYposition = Random.Range(columnmin, columnmax);
            columns[currentColumn].transform.position = new Vector2(spawnXposition, spawnYposition);
            currentColumn++;
            if (currentColumn >= columnpoolsize)
            {
                currentColumn = 0;
            }
        }
    }
}
