﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameControl : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        if(instance== null)
        {
            instance = this;
        }else if (instance!=this)
        {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(gameOver== true && Input.GetMouseButtonDown(0))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    public GameObject GameOverText;
    public bool gameOver = false;
    public static GameControl instance;
    public float scrollspeed = -1.5f;
    private int score = 0;
    public Text scoretext;
    public void BirdScore()
    {
        if (gameOver)
        {
            return;
        }
        score++;
        scoretext.text = "Score:" + score.ToString();

    }
    public void BirdDie()
    {
        GameOverText.SetActive(true);
        gameOver = true;

    }
}
